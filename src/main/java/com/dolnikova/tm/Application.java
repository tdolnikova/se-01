package com.dolnikova.tm;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Application {

    private static Scanner scanner;
    private static List<Project> projects = new ArrayList<>();

    private static final String HELP = "help";
    private static final String CREATE_PROJECT = "create project";
    private static final String READ_PROJECT = "read project";
    private static final String UPDATE_PROJECT = "update project";
    private static final String DELETE_PROJECT = "delete project";
    private static final String CREATE_TASK = "create task";
    private static final String READ_TASK = "read task";
    private static final String UPDATE_TASK = "update task";
    private static final String DELETE_TASK = "delete task";
    private static final String EXIT = ">> press enter <<";
    private static final String HELP_STRING = HELP + ": show all commands\n"
            + CREATE_PROJECT + ": create a project\n" + READ_PROJECT + ": read a project\n" + UPDATE_PROJECT + ": update a project\n" + DELETE_PROJECT + ":delete a project\n"
            + CREATE_TASK + ": create a task\n" + READ_TASK + ": read a task\n" + UPDATE_TASK + ": update a task\n" + DELETE_TASK + ": delete a task\n" + EXIT + ": exit";

    public static void main(String[] args) {
        System.out.println("* * * Welcome to Task Manager * * *");
        scanner = new Scanner(System.in);
        String input = "project";
        while (!input.isEmpty()) {
            input = scanner.nextLine();
            switch (input) {
                case HELP:
                    showHelp();
                    break;
                case CREATE_PROJECT:
                    createProject();
                    break;
                case READ_PROJECT:
                    readProject();
                    break;
                case UPDATE_PROJECT:
                    updateProject();
                    break;
                case DELETE_PROJECT:
                    deleteProject();
                    break;
                case CREATE_TASK:
                    createTask();
                    break;
                case READ_TASK:
                    readTask();
                    break;
                case UPDATE_TASK:
                    updateTask();
                    break;
                case DELETE_TASK:
                    deleteTask();
                    break;
                default:
                    if (!input.isEmpty())
                        System.out.println("Такой команды не существует");
                    break;
            }
        }
    }

    private static void showHelp() {
        System.out.println(HELP_STRING);
    }

    private static void createProject() {
        System.out.println("Введите название нового проекта.");
        boolean found = false;
        while (!found) {
            String projectName = scanner.nextLine();
            if (projectName.isEmpty()) break;
            for (Project project : projects) {
                if (projectName.equals(project.getProjectName())) break;
                System.out.println("Проект с таким названием уже существует.");
                found = true;
                break;
            }
            if (!found) {
                projects.add(new Project(projectName));
                System.out.println("Проект " + projectName + " создан.");
                break;
            }
        }
    }

    private static void readProject() {
        if (projects.isEmpty()) {
            System.out.println("Проектов нет.");
            return;
        }
        System.out.println("Введите название проекта");
        boolean fileFound = false;
        boolean tasksFound = false;
        boolean projectRead = false;
        while (!projectRead) {
            String projectName = scanner.nextLine();
            if (projectName.isEmpty()) break;
            for (Project project : projects) {
                if (projectName.equals(project.getProjectName())) {
                    fileFound = true;
                    if (project.getTasks().size() != 0) tasksFound = true;
                    for (int i = 0; i < project.getTasks().size(); i++) {
                        System.out.println("#" + (i + 1) + " " + project.getTasks().get(i).getTaskText());
                    }
                    projectRead = true;
                }
            }

            if (!fileFound) System.out.println("Проекта " + projectName + " не существует. Попробуйте еще раз.");
            else if (!tasksFound) {
                System.out.println("В проекте " + projectName + " нет задач");
                projectRead = true;
            } else projectRead = true;
        }
    }

    private static void createTask() {
        System.out.println("Выберите проект");
        boolean taskCreated = false;
        boolean fileFound = false;
        while (!taskCreated) {
            String projectName = scanner.nextLine();
            for (Project project : projects) {
                if (projectName.equals(project.getProjectName())) {
                    fileFound = true;
                    System.out.println("Введите задачу: ");
                    String taskText = "task";
                    while(!taskCreated) {
                        taskText = scanner.nextLine();
                        if (taskText.isEmpty()) taskCreated = true;
                        else {
                            project.addTask(new Task(taskText));
                            System.out.println("Задача " + taskText + " добавлена.");
                        }
                    }
                }
            }
            if (!fileFound) {
                System.out.println("Проекта с таким названием не существует.");
                taskCreated = true;
            }
        }
    }

    private static void updateProject() {
        System.out.println("Введите название проекта.");
        boolean fileFound = false;
        while (!fileFound) {
            String projectName = scanner.nextLine();
            for (Project project : projects) {
                if (projectName.equals(project.getProjectName())) {
                    fileFound = true;
                    System.out.println("Введите новое название проекта.");
                    while (true) {
                        String newProjectName = scanner.nextLine();
                        if (!newProjectName.isEmpty()) {
                            project.setProjectName(newProjectName);
                            System.out.println("Проект " + newProjectName + " обновлен.");
                            break;
                        }
                    }
                }
            }
            if (!fileFound) System.out.println("Проекта " + projectName + " не существует. Попробуйте еще раз.");
        }
    }

    private static void deleteProject() {
        if (projects.isEmpty()) {
            System.out.println("Проектов нет.");
            return;
        }
        System.out.println("Какой проект хотите удалить?");
        Project project = selectProject();
        if (project != null) {
            projects.remove(project);
            System.out.println("Проект удален.");
        }
    }

    private static Project selectProject() {
        String projectName;
        while (true) {
            projectName = scanner.nextLine();
            if (projectName.isEmpty()) return null;
            for (Project project : projects) {
                if (projectName.equals(project.getProjectName())) return project;
            }
            System.out.println("Проекта " + projectName + " не существует. Попробуйте еще раз.");
        }
    }

    private static void readTask() {
        System.out.println("Введите название проекта.");
        Project project = selectProject();
        if (project != null) {
            System.out.println("В проекте " + project.getProjectName() + " " + project.getTasks().size() + " задач. Введите номер задачи.");
            boolean taskRead = false;
            while (!taskRead) {
                String taskNumber = scanner.nextLine();
                if (taskNumber.isEmpty()) break;
                if (isNumber(taskNumber)) {
                    int num = Integer.parseInt(taskNumber);
                    if (taskExists(num, project.getTasks().size())) {
                        System.out.println("#" + num + " " + project.readTask(num).getTaskText());
                        taskRead = true;
                    }
                    else System.out.println("Некорректный ввод числа.");
                } else
                    System.out.println("Некорректный ввод числа.");
            }
        }
    }

    private static boolean isNumber(String strNum) {
        try {
            Integer.parseInt(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    private static void updateTask() {
        System.out.println("Введите название проекта.");
        Project project = selectProject();
        if (project != null) {
            int taskNumber = project.getTasks().size();
            if (taskNumber == 0) {
                System.out.println("В проекте нет задач");
                return;
            }
            System.out.println("В проекте " + project.getProjectName() + " "
                    + taskNumber + " задач. Введите номер задачи для редактирования.");
            boolean taskUpdated = false;
            while (!taskUpdated) {
                String number = scanner.nextLine();
                if (number.isEmpty()) break;
                if (isNumber(number)) {
                    int num = Integer.parseInt(number);
                    if (taskExists(num, taskNumber)) {
                        System.out.println("Введите новую задачу");
                        String newTaskText = scanner.nextLine();
                        if (newTaskText.isEmpty()) break;
                        project.updateTask(num, new Task(newTaskText));
                        System.out.println("Задача изменена.");
                        taskUpdated = true;
                    } else
                        System.out.println("Попробуйте ввести число еще раз");
                } else System.out.println("Попробуйте ввести число еще раз");
            }
        }
    }

    private static void deleteTask() {
        System.out.println("Введите название проекта.");
        Project project = selectProject();
        if (project != null) {
            int taskNumber = project.getTasks().size();
            System.out.println("В проекте " + taskNumber + " задач. Введите номер задачи для удаления.");
            boolean taskDeleted = false;
            while (!taskDeleted) {
                String number = scanner.nextLine();
                if (number.isEmpty()) break;
                if (isNumber(number)) {
                    int num = Integer.parseInt(number);
                    if (taskExists(num, taskNumber)) {
                        project.deleteTask(num);
                        System.out.println("Задача удалена.");
                        taskDeleted = true;
                    }
                }
            }
        }
    }

    private static boolean taskExists(int input, int projectSize) {
        return input > 0 && input <= projectSize;
    }
}
